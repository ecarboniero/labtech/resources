## How to install keycloak

This is a tutorial for install a keycloak cluster inside a k8s cluster.

### Requirements
1. A k8s cluster up and running.
2. kubectl installed and configured.
3. A postgres instance available. If you need one click [here](../postgres/README.md)

Follow this instructions:
```bash
kubectl apply -f keycloak.yaml
```
 If you whant to expose keycloak with **http** use the following command:
```bash 
kubectl apply -f ingress-http.yaml
```
 If you whant to expose keycloak with **https** use the following command:
 ```bash
kubectl apply -f ingress-https.yaml
```