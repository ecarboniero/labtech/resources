## How to install tls certificate for ingress

This is a tutorial for install a tls certificate for ingress.

### Requirements
1. A k8s cluster up and running
2. kubectl installed and configured

Follow this instructions:
```bash
openssl req -x509 -nodes -days 3650 -newkey rsa:4096 -keyout labtech.local.key  -out labtech.local.crt
kubectl create secret tls labtech.local --key labtech.local.key --cert labtech.local.crt -n iam
```