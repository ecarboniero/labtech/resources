## How to install nginx ingress controller

This is a tutorial for install a nginx ingress controller via daemonset implementation inside a k8s cluster.

### Requirements
1. A k8s cluster up and running
2. kubectl installed and configured

Follow this instructions:
```bash
kubectl apply -f 1-ns-and-sa.yaml
kubectl apply -f 2-rbac.yaml
kubectl apply -f 3-ap-rbac.yaml
kubectl apply -f 4-default-server-secret.yaml
kubectl apply -f 5-nginx-config.yaml
kubectl apply -f 6-ingress-class.yaml
kubectl apply -f 7-k8s.nginx.org_virtualservers.yaml
kubectl apply -f 8-k8s.nginx.org_virtualserverroutes.yaml
kubectl apply -f 9-k8s.nginx.org_transportservers.yaml
kubectl apply -f 10-k8s.nginx.org_policies.yaml
kubectl apply -f 11-nginx-ingress.yaml
```
Now the nginx ingress controller is installed inside the k8s cluster. Enjoy with it.