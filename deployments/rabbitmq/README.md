## How to install a rabbitmq cluster via operator

This is a tutorial for install a rabbitmq cluster via operator instance inside a k8s cluster.

### Requirements
1. A k8s cluster up and running
2. kubectl installed and configured

Follow this instructions:
```bash
kubectl apply -f cluster-operator.yml
```