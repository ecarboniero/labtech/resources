## How to install postgres

This is a tutorial for install a postgres instance via statefulset implementation inside a k8s cluster.

### Requirements
1. A k8s cluster up and running
2. kubectl installed and configured

Follow this instructions:
```bash
kubectl apply -f postgres.yaml
```