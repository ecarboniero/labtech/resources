# Labtech resources
This project aims to describe the procedures for setting up a development environment based on kubernetes.
## Requirements
This tutorial is based on a virtual machine based on *Virtual Box* with an ubuntu 20.04 linux distro with at minimum:
* 2 CPU
* 4GB RAM
* 2 network adapters; the first one configured like a "Host Only Adapter" while the second one configured with NAT.
## Assumptions
The "Host Only" network adapter is configured with a **fixed ipv4 address** like this: *192.168.56.20*.
Following the screenshot with my network configuration:


![Network Configuration](images/hostOnlyAdapterConfiguration.png)
>The following documentations refere to this address so, **if you change it, keep attention and when is used, change with yours**.
### *Docker installation*
```bash
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt update
sudo apt-get install docker-ce=5:19.03.15~3-0~ubuntu-focal docker-ce-cli=5:19.03.15~3-0~ubuntu-focal
sudo systemctl status docker
sudo systemctl enable docker
sudo usermod -aG docker ${USER}
sudo apt-mark hold docker-ce docker-ce-cli
sudo reboot
```
Verify the correct functionality of docker with ***docker ps*** or ***docker images*** commands.
### *Kubernetes Control (kubectl) installation*
```bash
sudo apt-get update && sudo apt-get install -y apt-transport-https gnupg2 curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl
sudo apt-mark hold kubectl
```
Verify the correct functionality of kubectl with ***kubectl version --client*** command.
### *Kubernetes (k3s) installation*
```bash
sudo curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=v1.19.7+k3s1 INSTALL_K3S_EXEC="server --advertise-address 192.168.56.20 --docker --disable traefik,metrics-server"  sh -
cd
mkdir .kube
sudo cp /etc/rancher/k3s/k3s.yaml .kube/config
sudo chown devuser:devuser .kube/config
sudo chmod 644 .kube/config
```
Verify the correct functionality of k3s with ***kubectl get nodes*** or ***kubectl get po -A*** commands.
### *Kubernetes (k3s) additional components*
If you want to install other components like these:
1. **nginx ingress controller**: click [here](deployments/ingress/README.md)
2. **tls certificate**: click [here](deployments/tls/README.md)   
3. **postgres instance**: click [here](deployments/postgres/README.md)
4. **keycloak cluster**: click [here](deployments/keycloak/README.md)
5. **rabbitmq cluster**: click [here](deployments/rabbitmq/README.md)